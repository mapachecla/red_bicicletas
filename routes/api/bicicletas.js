var express = require('express');
var bicicletaController = require('../../controllers/api/bicicletaControllerAPI');

var router = express.Router();

router.get('/', bicicletaController.bicicleta_list);
router.post('/create', bicicletaController.bicicleta_create);
router.post('/update', bicicletaController.bicicleta_update);
router.delete('/delete', bicicletaController.bicicleta_delete);


module.exports = router;