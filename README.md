# Proyecto Red de bicicletas

## Instalación

* Descargue el .Zip o clone el repositorio con `git clone https://mapachecla@bitbucket.org/mapachecla/red_bicicletas.git`
* Vaya al directorio red_bicicletas y ejecute en consola el comando `npm install`, esto instalará las dependencias del proyecto.

## Uso

* Ejecute el comando `npm run devstart` y escriba en el navegador `http://localhost:3000`
