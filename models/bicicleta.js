// Objecto Bicicleta
var Bicicleta = function(id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function() {
    return 'id: ' + this.id + " | color: " + this.color;
}

// Metodo agregar bicicletas
Bicicleta.allBicis = []; // A Bicicleta le agregamos una propiedad que va a ser un array llamado allBicis
Bicicleta.add = function(aBici) {
    Bicicleta.allBicis.push(aBici)
}

// Metodo buscar bicicletas
Bicicleta.findById = function(aBiciId) {
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici)
        return aBici;
    else
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
}

// Metodo borrar bicicletas
Bicicleta.removeById = function(aBiciId) {
    for (var i = 0; i < Bicicleta.allBicis.length; i++) {
        if (Bicicleta.allBicis[i].id == aBiciId) {
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}

// Metodo actualizar bicicletas
Bicicleta.updateBici = function(newBici) {
    for (let i = 0; i < Bicicleta.allBicis.length; i++) {
        if (newBici.id == Bicicleta.allBicis[i].id) {

            Bicicleta.allBicis[i].color = newBici.color;
            Bicicleta.allBicis[i].modelo = newBici.modelo;
            Bicicleta.allBicis[i].ubicacion = newBici.ubicacion;
        }
    }
}


// var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
// var b = new Bicicleta(2, 'blanca', 'urbana', [-34.596932, -58.3808287]);

// Bicicleta.add(a);
// Bicicleta.add(b);

module.exports = Bicicleta;